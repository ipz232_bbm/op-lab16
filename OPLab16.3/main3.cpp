#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

int size(int n) {
	int size = 0;
	while (n != 0) {
		n /= 10;
		size++;
	}
	return size;
}

int reverse(int n) {
	if (n == 0) return 0;
	return n%10 * pow(10, size(n) - 1) + reverse(n/10);
}

int main() {
	int n;
	scanf("%d", &n);

	int reversedN = reverse(n);

	printf("%d", reversedN);

	return 0;
}