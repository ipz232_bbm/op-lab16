#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int summa(int mas[], int n) {
	int sum = 0;
	for (int i = 0; i < n; i++) {
		sum += mas[i];
	}
	return sum;
}

long long productFunc(int mas[], int n) {
	long long product = 1;
	for (int i = 0; i < n; i++) {
		product *= mas[i];
	}
	return product;
}

int max(int mas[], int n) {
	int max = mas[0];
	for (int i = 1; i < n; i++) {
		if (mas[i] > max) max = mas[i];
	}
	return max;
}

int min(int mas[], int n) {
	int min = mas[0];
	for (int i = 1; i < n; i++) {
		if (mas[i] < min) min = mas[i];
	}
	return min;
}

void generate_Array(int *arr, int n){

	for (int i = 0; i < n; i++) {
		arr[i] = rand() % 100;
		printf("%2d ", arr[i]);
	}

	}

int main() {
	srand(time(NULL));

	int arr[10], n = 10;

	generate_Array(arr, n);

	int sum = summa(arr, n);
	printf("\nsumma = %d", sum);

	int maximum = max(arr, n);
	printf("\nMaximum = %d", maximum);

	int minimum = min(arr, n);
	printf("\nMinimum = %d", minimum);

	long long product = productFunc(arr, n);
	printf("\nProduct = %lld", product);

	return 0;
}