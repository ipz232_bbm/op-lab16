#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

double P(double a, double b, double c, double d) {
	return a + b + c + d;
}

int main() {
	double a, b, c, d;

	printf("a = ");
	scanf("%lf", &a);
	printf("b = ");
	scanf("%lf", &b);
	printf("c = ");
	scanf("%lf", &c);
	printf("d = ");
	scanf("%lf", &d);

	double p = P(a, b, c, d);

	printf("%lf", p);

	return 0;
}